//soal 1
//Jawaban soal 1
let p = 2
let l = 3
const luas = (p, l) => "luas= "+ p * l;
const keliling = (p,l) => "keliling= "+ 2*(p+l);
console.log(luas(p,l));
console.log(keliling(p,l));

console.log("===================================")

//soal 2
//jawaban soal 2
const newFunction = (firstName, lastName)=>({firstName,lastName, fullName: function(){
    console.log(`${firstName} ${lastName}`)
    }
}
);
    newFunction("William", "Imoh").fullName()
    console.log("===================================")

//soal 3
//jawaban soal 3
let studentName = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
};

const {firstName, lastName, address, hobby} = studentName

console.log(firstName, lastName, address, hobby);

console.log("===================================")

//soal 4
//jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]

console.log(combined)

console.log("===================================")

// soal 5
// jawaban soal 5
const planet = "earth" 
const view = "glass" 

const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`

console.log(before)
