// Soal 1
// buatlah variabel-variabel seperti di bawah ini
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
// gabungkan variabel-variabel tersebut agar menghasilkan output
// saya senang belajar JAVASCRIPT

//Jawaban Soal 1
var saya = pertama.substring(0, 4);
var senang = pertama.substring(12, 18);
var belajar = kedua.substring(0, 7);
var jv= kedua.substring(8, 18);
var jvup = jv.toUpperCase();
console.log(saya.concat(' ').concat(senang).concat(' ').concat(belajar).concat(' ').concat(jvup));

// Soal 2
// buatlah variabel-variabel seperti di bawah ini
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).

//Jawaban soal 2
//mengubah string ke integer
var intStr1 = parseInt(kataPertama);
var intStr2 = parseInt(kataKedua);
var intStr3 = parseInt(kataKetiga);
var intStr4 = parseInt(kataKeempat);
//operasi matematika
var operasi = intStr1%intStr2+intStr3*intStr4;
//output
console.log(operasi, typeof(operasi));

//Soal 3
//buatlah variabel-variabel seperti di bawah ini

var kalimat = 'wah javascript itu keren sekali'; 
//Jawaban soal 3
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);  
var kataKetiga = kalimat.substring(15, 18);  
var kataKeempat = kalimat.substring(19, 24);  
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);