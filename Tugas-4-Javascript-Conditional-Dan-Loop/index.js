// soal 1

// buatlah variabel seperti di bawah ini

// var nilai;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi
// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

// Jawaban soal 1

var nilai = 100;

if ( nilai >= 85) {
    console.log ('indeksnya A')
}
else if (nilai >=75 && nilai < 85){
        console.log('indeksnya B')
    }
else if (nilai >=65 && nilai < 75){
        console.log('indeksnya C')
    }
else if (nilai >=55 && nilai < 65){
        console.log('indeksnya D')
    }
else {
        console.log ('Belajar Lebih Giat Lagi')
    }

console.log ('------------------------------------')

// Soal 2

// buatlah variabel seperti di bawah ini

// var tanggal = 22;
// var bulan = 7;
// var tahun = 2020;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, 
// lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

//Jawaban soal 2

var tanggal = 2;
var bulan = 6;
var tahun = 1994;

switch(bulan) {
    case 1:   { console.log(tanggal, 'Januari', tahun); break; }
    case 2:   { console.log(tanggal,'Februari', tahun); break; }
    case 3:   { console.log(tanggal,'Maret', tahun); break; }
    case 4:   { console.log(tanggal,'April', tahun); break; }
    case 5:   { console.log(tanggal,'Mei', tahun); break; }
    case 6:   { console.log(tanggal,'Juni', tahun); break; }
    case 7:   { console.log(tanggal,'Juli', tahun); break; }
    case 8:   { console.log(tanggal,'Agustus', tahun); break; }
    case 9:   { console.log(tanggal,'September', tahun); break; }
    case 10:   { console.log(tanggal,'Oktober', tahun); break; }
    case 11:   { console.log(tanggal,'November', tahun); break; }
    case 12:   { console.log(tanggal,'Desember', tahun); break; }
}

console.log ('------------------------------------')

// Soal 3

// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

//Jawaban Soal 3

// Output untuk n=3 :

var hashtag3 = ''
for( var tinggi = 0; tinggi < 3; tinggi++ ){
    for ( var alas = 0; alas <= tinggi; alas++){
        hashtag3 += '#';
    }
    hashtag3 += '\n';
}
console.log(hashtag3);

// Output untuk n=7 :

var hashtag7 = ''
for( var tinggi = 0; tinggi < 7; tinggi++ ){
    for ( var alas = 0; alas <= tinggi; alas++){
        hashtag7 += '#';
    }
    hashtag7 += '\n';
}
console.log(hashtag7);

console.log ('------------------------------------')

// Soal 4

// berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.

//Jawaban Soal 4

// Output untuk m = 3
for(var angka = 0; angka <= 3; angka++) {

    if(angka == 3 || angka == 6 || angka == 9){
    console.log(angka+' - I love VueJs')
    if (angka == 3){
            console.log('===')
    }
    if (angka == 6) {
            console.log ('======')
    }
    if (angka == 9) {
            console.log ('=========')
    }
    }
    if (angka == 1 || angka == 4 || angka == 7 || angka == 10){
    console.log (angka+' - I love Programming')
    }
    if (angka == 2 || angka == 5 || angka == 8){
    console.log (angka+' - I love Javascript')
    }
}
console.log ('------------------------------------')
// Output untuk m = 5
for(var angka = 0; angka <= 5; angka++) {

    if(angka == 3 || angka == 6 || angka == 9){
    console.log(angka+' - I love VueJs')
    if (angka == 3){
            console.log('===')
    }
    if (angka == 6) {
            console.log ('======')
    }
    if (angka == 9) {
            console.log ('=========')
    }
    }
    if (angka == 1 || angka == 4 || angka == 7 || angka == 10){
    console.log (angka+' - I love Programming')
    }
    if (angka == 2 || angka == 5 || angka == 8){
    console.log (angka+' - I love Javascript')
    }
}
console.log ('------------------------------------')
// Output untuk m = 7
for(var angka = 0; angka <= 7; angka++) {

    if(angka == 3 || angka == 6 || angka == 9){
    console.log(angka+' - I love VueJs')
    if (angka == 3){
            console.log('===')
    }
    if (angka == 6) {
            console.log ('======')
    }
    if (angka == 9) {
            console.log ('=========')
    }
    }
    if (angka == 1 || angka == 4 || angka == 7 || angka == 10){
    console.log (angka+' - I love Programming')
    }
    if (angka == 2 || angka == 5 || angka == 8){
    console.log (angka+' - I love Javascript')
    }
}
console.log ('------------------------------------')
// Output untuk m = 10
for(var angka = 0; angka <= 10; angka++) {

    if(angka == 3 || angka == 6 || angka == 9){
    console.log(angka+' - I love VueJs')
    if (angka == 3){
            console.log('===')
    }
    if (angka == 6) {
            console.log ('======')
    }
    if (angka == 9) {
            console.log ('=========')
    }
    }
    if (angka == 1 || angka == 4 || angka == 7 || angka == 10){
    console.log (angka+' - I love Programming')
    }
    if (angka == 2 || angka == 5 || angka == 8){
    console.log (angka+' - I love Javascript')
    }
}